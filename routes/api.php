<?php

use Illuminate\Http\Request;
use app\Test;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/users', function (Request $request) {
    return $request->user();
});

Route::prefix('sign')->group(function () {
	Route::post('in',  'Signing@signin');
  Route::post('up', 'Signing@signup');
	Route::get('verify', 'Signing@verify');
  Route::get('refresh', 'Signing@refresh');
  Route::middleware('auth:api')->post('out','Signing@signout');
});

Route::middleware('auth:api')->get('ireports','Ireports@get');
Route::middleware('auth:api')->get('videobites','Videobites@get');

Route::middleware('auth:api')->post('test', 'Test@testing');
