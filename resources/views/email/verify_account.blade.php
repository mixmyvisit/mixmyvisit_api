<h3>Click the Link To Verify Your Email</h3>
Click the following link to verify your email {{ url('api/sign/verify?verification_token='. $verification_token) }}
