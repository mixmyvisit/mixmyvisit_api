<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\Models\User;
use App\Http\Controllers\Auth\LoginProxy;
use App\Jobs\SendVerificationEmail;

use Carbon\Carbon;
use Validator;


class Signing extends Controller
{

	private $loginProxy;

    public function __construct(LoginProxy $loginProxy)
    {
        $this->loginProxy = $loginProxy;
    }

    /**
     * Handle an authentication attempt.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return Response
    */
 	public function signin(Request $request)
    {
				$request->validate([
                'email'    => 'required|email|max:180',
                'password' => 'required|string|max:255'
        ]);

				 $user = User::where('email', $request -> email)->first();

				//check if valid credentials exists else send that exception
        if (!isset($user)) {
					dd('USER DOES NOT EXIST');
				}

				//check if users is confirmed else send that exception
				if(!isset($user->email_verified_at)){
					dd('USER NOT CONFIMED');
				}

				//check if users is blocked else send that exception
				if($user->user_states_id != 1){
					dd('USER BLOCKED');
				}

        //Attempt to login with password else send that exception
        $login = $this->loginProxy->attemptLogin($request -> email, $request -> password);

        if($login == 'error'){
					dd($login);
				}

        return response([
            'access_token' => $login['access_token'],
            'expires_in'   => $login['expires_in']
        ]
        )->cookie($login['cookie']);

    }

    public function signup(Request $request)
    {
        $request->validate([
                'name'            => 'required|string|unique:users|max:180',
                'firstname'       => 'required|string|max:180',
                'lastname'        => 'required|string|max:180',
                'email'           => 'required|email|unique:users|max:180',
                'birthday'        => 'required|date',
                'password'        => 'required|string|max:255',
                'profile_picture' => 'nullable|mimes:jpeg,bmp,png'
        ]);

        $user = User::create([
            'name' => $request -> name,
            'firstname' => $request -> firstname,
            'lastname' => $request -> lastname,
            'email' => $request -> email,
            'birthday' => new Carbon($request -> birthday),
            'password' => Hash::make($request -> password),
            'verification_token' => Hash::make($request -> email),
            'user_types_id' => 1
        ]);

        if(!$user){
            dd('THROW ERROR');
        }

        SendVerificationEmail::dispatch($user)->onQueue('emails');;

        return response("Email verification Sent");
    }


    /**
     * Handle an authentication attempt.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return Response
    */
    public function verify(Request $request)
    {
        $user = User::where('verification_token', $request -> verification_token)->first();
        $user->email_verified_at =  Carbon::now();
				$user->save();
        return response($user, 200);
    }

    public function refresh(Request $request)
    {
        return response($this->loginProxy->attemptRefresh());
    }

    public function signout()
    {
        $logout = $this->loginProxy->logout();

        return response(null, 204)->cookie($logout['cookie']);
    }
}
