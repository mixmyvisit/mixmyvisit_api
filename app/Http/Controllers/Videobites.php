<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Validator;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class Videobites extends Controller
{

    /**
     * Handle an authentication attempt.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return Response
    */
 	public function get(Request $request)
    {
    	$client = new Client(); //GuzzleHttp\Client
    	$client->setDefaultOption('headers', 
    		array(
    			'Host' => '127.0.0.1:35725',
    			'Accept-Encoding' => 'gzip, deflate',
    			'User-Agent' => 'node-superagent/2.3.0',
    			'Content-Type' => 'application/json',
    			'Connection' => 'close',
    			'authorizaion' => `Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1Mzk3ODc0NTksImlhdCI6MTUzODU3Nzg1OSwic3ViIjp7InVzZXJfaWQiOjF9fQ.pPmHlUQe29L-Kr2IynhtlJc_6NK0cztW8_BLhaXAUUY`
    		)
    	);

		$res = $client->request('GET', 'dev2-videobite.westeurope.cloudapp.azure.com:3030/api/videobites');


    }

}
