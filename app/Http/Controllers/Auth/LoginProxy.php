<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Cookie;
use  App\Models\User;

//use Infrastructure\Auth\Exceptions\InvalidCredentialsException;

class LoginProxy
{
    const REFRESH_TOKEN = 'refreshToken';

    private $apiConsumer;

    private $cookie;

    private $db;

    private $request;


    public function __construct(Application $app) {
        $this->apiConsumer = $app->make('apiconsumer');
        $this->auth = $app->make('auth');
        $this->db = $app->make('db');
        $this->request = $app->make('request');
    }

    /**
     * Attempt to create an access token using user credentials
     *
     * @param string $email
     * @param string $password
     */
    public function attemptLogin($email, $password)
    {

        return $this->proxy('password', [
            'username' => $email,
            'password' => $password
        ]);

    }

    /**
     * Attempt to refresh the access token used a refresh token that
     * has been saved in a cookie
     */
    public function attemptRefresh()
    {
        $refreshToken = $this->request->cookie(self::REFRESH_TOKEN);

        return $this->proxy('refresh_token', [
            'refresh_token' => $refreshToken
        ]);
    }

    /**
     * Proxy a request to the OAuth server.
     *
     * @param string $grantType what type of grant type should be proxied
     * @param array $data the data to send to the server
     */
    public function proxy($grantType, array $data = [])
    {

        $data = array_merge($data, [
            'client_id'     => env('PASSWORD_CLIENT_ID'),
            'client_secret' => env('PASSWORD_CLIENT_SECRET'),
            'grant_type'    => $grantType
        ]);

        $response = $this->apiConsumer->post('/oauth/token', $data);

        if (!$response->isSuccessful()) {
             return('error');
            //throw new InvalidCredentialsException();
        }

        $data = json_decode($response->getContent());

        //data to attach httpOnly cookie to response
        $cookie = Cookie::make(self::REFRESH_TOKEN, $data->refresh_token, 864000 /*10days*/ , null, null, false, true);

        return [
            'access_token' => $data->access_token,
            'expires_in'   => $data->expires_in,
            'cookie'       => $cookie
        ];
    }

    /**
     * Logs out the user. We revoke access token and refresh token.
     * Also instruct the client to forget the refresh cookie.
     */
    public function logout()
    {
        $accessToken = $this->auth->user()->token();

        $refreshToken = $this->db
            ->table('oauth_refresh_tokens')
            ->where('access_token_id', $accessToken->id)
            ->update([
                'revoked' => true
        ]);

        $accessToken->revoke();

        $cookie = Cookie::forget(self::REFRESH_TOKEN);

        return [
            'cookie' => $cookie
        ];
    }
}
