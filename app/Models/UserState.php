<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 11 Oct 2018 19:14:52 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UserState
 * 
 * @property int $id
 * @property string $state
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $admin_blocks_users
 * @property \Illuminate\Database\Eloquent\Collection $users
 *
 * @package App\Models
 */
class UserState extends Eloquent
{
	protected $fillable = [
		'state'
	];

	public function admin_blocks_users()
	{
		return $this->hasMany(\App\Models\AdminBlocksUser::class, 'previous_states_id');
	}

	public function users()
	{
		return $this->hasMany(\App\Models\User::class, 'user_states_id');
	}
}
