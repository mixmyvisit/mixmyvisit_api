<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 11 Oct 2018 19:14:52 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UsersAchievement
 * 
 * @property int $users_id
 * @property int $achievements_id
 * @property string $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Achievement $achievement
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class UsersAchievement extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	public $incrementing = false;

	protected $casts = [
		'users_id' => 'int',
		'achievements_id' => 'int'
	];

	public function achievement()
	{
		return $this->belongsTo(\App\Models\Achievement::class, 'achievements_id');
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'users_id');
	}
}
