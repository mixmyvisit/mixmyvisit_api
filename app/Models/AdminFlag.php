<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 11 Oct 2018 19:14:52 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class AdminFlag
 * 
 * @property int $id
 * @property int $users_id
 * @property int $flags_id
 * @property int $previous_states_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\State $state
 * @property \App\Models\Flag $flag
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class AdminFlag extends Eloquent
{
	protected $casts = [
		'users_id' => 'int',
		'flags_id' => 'int',
		'previous_states_id' => 'int'
	];

	protected $fillable = [
		'users_id',
		'flags_id',
		'previous_states_id'
	];

	public function state()
	{
		return $this->belongsTo(\App\Models\State::class, 'previous_states_id');
	}

	public function flag()
	{
		return $this->belongsTo(\App\Models\Flag::class, 'flags_id');
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'users_id');
	}
}
