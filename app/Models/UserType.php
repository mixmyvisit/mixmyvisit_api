<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 11 Oct 2018 19:14:52 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UserType
 * 
 * @property int $id
 * @property string $type
 * @property int $necessary_points
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $users
 *
 * @package App\Models
 */
class UserType extends Eloquent
{
	protected $casts = [
		'necessary_points' => 'int'
	];

	protected $fillable = [
		'type',
		'necessary_points'
	];

	public function users()
	{
		return $this->hasMany(\App\Models\User::class, 'user_types_id');
	}
}
