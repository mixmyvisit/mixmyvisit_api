<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 11 Oct 2018 19:14:52 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class FlagsType
 * 
 * @property int $id
 * @property string $type
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $flags
 *
 * @package App\Models
 */
class FlagsType extends Eloquent
{
	protected $fillable = [
		'type'
	];

	public function flags()
	{
		return $this->hasMany(\App\Models\Flag::class, 'flags_types_id');
	}
}
