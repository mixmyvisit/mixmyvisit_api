<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 11 Oct 2018 19:14:52 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Challenge
 * 
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $achieving_points
 * @property int $achieving_badge
 * @property string $conditions
 * @property string $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Badge $badge
 * @property \Illuminate\Database\Eloquent\Collection $assets
 * @property \Illuminate\Database\Eloquent\Collection $challenges_media
 * @property \Illuminate\Database\Eloquent\Collection $users
 *
 * @package App\Models
 */
class Challenge extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'achieving_badge' => 'int'
	];

	protected $fillable = [
		'name',
		'description',
		'achieving_points',
		'achieving_badge',
		'conditions'
	];

	public function badge()
	{
		return $this->belongsTo(\App\Models\Badge::class, 'achieving_badge');
	}

	public function assets()
	{
		return $this->belongsToMany(\App\Models\Asset::class, 'challenges_assets', 'challenges_id', 'assets_id')
					->withPivot('deleted_at')
					->withTimestamps();
	}

	public function challenges_media()
	{
		return $this->hasMany(\App\Models\ChallengesMedia::class, 'challenges_id');
	}

	public function users()
	{
		return $this->belongsToMany(\App\Models\User::class, 'users_challenges', 'challenges_id', 'users_id')
					->withPivot('completed', 'ireports_id', 'deleted_at')
					->withTimestamps();
	}
}
