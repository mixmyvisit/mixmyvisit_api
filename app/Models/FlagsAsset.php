<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 11 Oct 2018 19:14:52 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class FlagsAsset
 * 
 * @property int $flags_id
 * @property int $assets_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Asset $asset
 * @property \App\Models\Flag $flag
 *
 * @package App\Models
 */
class FlagsAsset extends Eloquent
{
	public $incrementing = false;

	protected $casts = [
		'flags_id' => 'int',
		'assets_id' => 'int'
	];

	public function asset()
	{
		return $this->belongsTo(\App\Models\Asset::class, 'assets_id');
	}

	public function flag()
	{
		return $this->belongsTo(\App\Models\Flag::class, 'flags_id');
	}
}
