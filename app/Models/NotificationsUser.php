<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 11 Oct 2018 19:14:52 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class NotificationsUser
 * 
 * @property int $notifications_id
 * @property int $users_id
 * @property bool $read
 * @property string $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Notification $notification
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class NotificationsUser extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	public $incrementing = false;

	protected $casts = [
		'notifications_id' => 'int',
		'users_id' => 'int',
		'read' => 'bool'
	];

	protected $fillable = [
		'read'
	];

	public function notification()
	{
		return $this->belongsTo(\App\Models\Notification::class, 'notifications_id');
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'users_id');
	}
}
