<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 11 Oct 2018 19:14:52 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Ireport
 * 
 * @property int $id
 * @property string $name
 * @property string $video_details
 * @property int $users_id
 * @property int $states_id
 * @property string $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\State $state
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $flags
 * @property \Illuminate\Database\Eloquent\Collection $highlights
 * @property \Illuminate\Database\Eloquent\Collection $assets
 * @property \Illuminate\Database\Eloquent\Collection $ireports_media
 * @property \Illuminate\Database\Eloquent\Collection $platforms
 * @property \Illuminate\Database\Eloquent\Collection $ireports_videos
 * @property \Illuminate\Database\Eloquent\Collection $users_challenges
 *
 * @package App\Models
 */
class Ireport extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'users_id' => 'int',
		'states_id' => 'int'
	];

	protected $fillable = [
		'name',
		'video_details',
		'users_id',
		'states_id'
	];

	public function state()
	{
		return $this->belongsTo(\App\Models\State::class, 'states_id');
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'users_id');
	}

	public function flags()
	{
		return $this->belongsToMany(\App\Models\Flag::class, 'flags_ireports', 'ireports_id', 'flags_id')
					->withTimestamps();
	}

	public function highlights()
	{
		return $this->hasMany(\App\Models\Highlight::class, 'ireports_id');
	}

	public function assets()
	{
		return $this->belongsToMany(\App\Models\Asset::class, 'ireports_assets', 'ireports_id', 'assets_id')
					->withPivot('id', 'deleted_at')
					->withTimestamps();
	}

	public function ireports_media()
	{
		return $this->hasMany(\App\Models\IreportsMedia::class, 'ireports_id');
	}

	public function platforms()
	{
		return $this->belongsToMany(\App\Models\Platform::class, 'ireports_platforms', 'iReports_id', 'platforms_id')
					->withPivot('description', 'likes', 'comments', 'shares', 'views', 'url', 'deleted_at')
					->withTimestamps();
	}

	public function ireports_videos()
	{
		return $this->hasMany(\App\Models\IreportsVideo::class, 'ireports_id');
	}

	public function users_challenges()
	{
		return $this->hasMany(\App\Models\UsersChallenge::class, 'ireports_id');
	}
}
