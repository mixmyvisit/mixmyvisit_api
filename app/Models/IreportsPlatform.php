<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 11 Oct 2018 19:14:52 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class IreportsPlatform
 * 
 * @property int $iReports_id
 * @property int $platforms_id
 * @property string $description
 * @property int $likes
 * @property int $comments
 * @property int $shares
 * @property int $views
 * @property string $url
 * @property string $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Ireport $ireport
 * @property \App\Models\Platform $platform
 *
 * @package App\Models
 */
class IreportsPlatform extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	public $incrementing = false;

	protected $casts = [
		'iReports_id' => 'int',
		'platforms_id' => 'int',
		'likes' => 'int',
		'comments' => 'int',
		'shares' => 'int',
		'views' => 'int'
	];

	protected $fillable = [
		'description',
		'likes',
		'comments',
		'shares',
		'views',
		'url'
	];

	public function ireport()
	{
		return $this->belongsTo(\App\Models\Ireport::class, 'iReports_id');
	}

	public function platform()
	{
		return $this->belongsTo(\App\Models\Platform::class, 'platforms_id');
	}
}
