<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 11 Oct 2018 19:14:52 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class MediaType
 * 
 * @property int $id
 * @property string $type
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $media
 *
 * @package App\Models
 */
class MediaType extends Eloquent
{
	protected $fillable = [
		'type'
	];

	public function media()
	{
		return $this->hasMany(\App\Models\Medium::class, 'media_types_id');
	}
}
