<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 11 Oct 2018 19:14:52 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ChallengesMedia
 * 
 * @property int $challenges_id
 * @property int $media_id
 * @property string $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Medium $medium
 * @property \App\Models\Challenge $challenge
 *
 * @package App\Models
 */
class ChallengesMedia extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	public $incrementing = false;

	protected $casts = [
		'challenges_id' => 'int',
		'media_id' => 'int'
	];

	public function medium()
	{
		return $this->belongsTo(\App\Models\Medium::class, 'media_id');
	}

	public function challenge()
	{
		return $this->belongsTo(\App\Models\Challenge::class, 'challenges_id');
	}
}
