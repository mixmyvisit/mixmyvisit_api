<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 11 Oct 2018 19:14:52 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Highlight
 * 
 * @property int $id
 * @property int $ireports_id
 * @property int $order
 * @property string $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Ireport $ireport
 *
 * @package App\Models
 */
class Highlight extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'ireports_id' => 'int',
		'order' => 'int'
	];

	protected $fillable = [
		'ireports_id',
		'order'
	];

	public function ireport()
	{
		return $this->belongsTo(\App\Models\Ireport::class, 'ireports_id');
	}
}
