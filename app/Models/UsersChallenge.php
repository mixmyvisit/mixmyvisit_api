<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 11 Oct 2018 19:14:52 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UsersChallenge
 * 
 * @property int $challenges_id
 * @property int $users_id
 * @property bool $completed
 * @property int $ireports_id
 * @property string $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\User $user
 * @property \App\Models\Challenge $challenge
 * @property \App\Models\Ireport $ireport
 *
 * @package App\Models
 */
class UsersChallenge extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	public $incrementing = false;

	protected $casts = [
		'challenges_id' => 'int',
		'users_id' => 'int',
		'completed' => 'bool',
		'ireports_id' => 'int'
	];

	protected $fillable = [
		'completed',
		'ireports_id'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'users_id');
	}

	public function challenge()
	{
		return $this->belongsTo(\App\Models\Challenge::class, 'challenges_id');
	}

	public function ireport()
	{
		return $this->belongsTo(\App\Models\Ireport::class, 'ireports_id');
	}
}
