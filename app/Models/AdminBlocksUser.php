<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 11 Oct 2018 19:14:52 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class AdminBlocksUser
 * 
 * @property int $id
 * @property int $admin
 * @property int $user
 * @property int $previous_states_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\UserState $user_state
 *
 * @package App\Models
 */
class AdminBlocksUser extends Eloquent
{
	protected $casts = [
		'admin' => 'int',
		'user' => 'int',
		'previous_states_id' => 'int'
	];

	protected $fillable = [
		'admin',
		'user',
		'previous_states_id'
	];

	public function user_state()
	{
		return $this->belongsTo(\App\Models\UserState::class, 'previous_states_id');
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'user');
	}
}
