<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 11 Oct 2018 19:14:52 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Medium
 * 
 * @property int $id
 * @property string $url
 * @property string $reference
 * @property int $media_types_id
 * @property int $assets_id
 * @property string $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Asset $asset
 * @property \App\Models\MediaType $media_type
 * @property \Illuminate\Database\Eloquent\Collection $badges
 * @property \Illuminate\Database\Eloquent\Collection $challenges_media
 * @property \Illuminate\Database\Eloquent\Collection $ireports_media
 * @property \Illuminate\Database\Eloquent\Collection $ireports_videos
 * @property \Illuminate\Database\Eloquent\Collection $media_assets
 * @property \Illuminate\Database\Eloquent\Collection $users
 * @property \Illuminate\Database\Eloquent\Collection $users_media
 *
 * @package App\Models
 */
class Medium extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'media_types_id' => 'int',
		'assets_id' => 'int'
	];

	protected $fillable = [
		'url',
		'reference',
		'media_types_id',
		'assets_id'
	];

	public function asset()
	{
		return $this->belongsTo(\App\Models\Asset::class, 'assets_id');
	}

	public function media_type()
	{
		return $this->belongsTo(\App\Models\MediaType::class, 'media_types_id');
	}

	public function badges()
	{
		return $this->hasMany(\App\Models\Badge::class, 'badge_picture');
	}

	public function challenges_media()
	{
		return $this->hasMany(\App\Models\ChallengesMedia::class, 'media_id');
	}

	public function ireports_media()
	{
		return $this->hasMany(\App\Models\IreportsMedia::class, 'media_id');
	}

	public function ireports_videos()
	{
		return $this->hasMany(\App\Models\IreportsVideo::class, 'media_id');
	}

	public function media_assets()
	{
		return $this->hasMany(\App\Models\MediaAsset::class, 'media_id');
	}

	public function users()
	{
		return $this->hasMany(\App\Models\User::class, 'profile_picture');
	}

	public function users_media()
	{
		return $this->hasMany(\App\Models\UsersMedia::class, 'media_id');
	}
}
