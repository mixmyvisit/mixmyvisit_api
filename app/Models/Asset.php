<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 11 Oct 2018 19:14:52 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Asset
 * 
 * @property int $id
 * @property string $endpoint
 * @property int $state_id
 * @property bool $premium
 * @property string $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\State $state
 * @property \Illuminate\Database\Eloquent\Collection $challenges
 * @property \Illuminate\Database\Eloquent\Collection $flags
 * @property \Illuminate\Database\Eloquent\Collection $ireports
 * @property \Illuminate\Database\Eloquent\Collection $media
 * @property \Illuminate\Database\Eloquent\Collection $media_assets
 * @property \Illuminate\Database\Eloquent\Collection $users
 *
 * @package App\Models
 */
class Asset extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'state_id' => 'int',
		'premium' => 'bool'
	];

	protected $fillable = [
		'endpoint',
		'state_id',
		'premium'
	];

	public function state()
	{
		return $this->belongsTo(\App\Models\State::class);
	}

	public function challenges()
	{
		return $this->belongsToMany(\App\Models\Challenge::class, 'challenges_assets', 'assets_id', 'challenges_id')
					->withPivot('deleted_at')
					->withTimestamps();
	}

	public function flags()
	{
		return $this->belongsToMany(\App\Models\Flag::class, 'flags_assets', 'assets_id', 'flags_id')
					->withTimestamps();
	}

	public function ireports()
	{
		return $this->belongsToMany(\App\Models\Ireport::class, 'ireports_assets', 'assets_id', 'ireports_id')
					->withPivot('id', 'deleted_at')
					->withTimestamps();
	}

	public function media()
	{
		return $this->hasMany(\App\Models\Medium::class, 'assets_id');
	}

	public function media_assets()
	{
		return $this->hasMany(\App\Models\MediaAsset::class, 'assets_id');
	}

	public function users()
	{
		return $this->belongsToMany(\App\Models\User::class, 'users_assets', 'assets_id', 'users_id')
					->withPivot('deleted_at')
					->withTimestamps();
	}
}
