<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 11 Oct 2018 19:14:52 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class FlagsIreport
 * 
 * @property int $flags_id
 * @property int $ireports_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Flag $flag
 * @property \App\Models\Ireport $ireport
 *
 * @package App\Models
 */
class FlagsIreport extends Eloquent
{
	public $incrementing = false;

	protected $casts = [
		'flags_id' => 'int',
		'ireports_id' => 'int'
	];

	public function flag()
	{
		return $this->belongsTo(\App\Models\Flag::class, 'flags_id');
	}

	public function ireport()
	{
		return $this->belongsTo(\App\Models\Ireport::class, 'ireports_id');
	}
}
