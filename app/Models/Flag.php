<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 11 Oct 2018 19:14:52 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Flag
 * 
 * @property int $id
 * @property int $flags_types_id
 * @property int $user_flagging_id
 * @property string $description
 * @property int $states_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\FlagsType $flags_type
 * @property \App\Models\State $state
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $admin_flags
 * @property \Illuminate\Database\Eloquent\Collection $assets
 * @property \Illuminate\Database\Eloquent\Collection $ireports
 *
 * @package App\Models
 */
class Flag extends Eloquent
{
	protected $casts = [
		'flags_types_id' => 'int',
		'user_flagging_id' => 'int',
		'states_id' => 'int'
	];

	protected $fillable = [
		'flags_types_id',
		'user_flagging_id',
		'description',
		'states_id'
	];

	public function flags_type()
	{
		return $this->belongsTo(\App\Models\FlagsType::class, 'flags_types_id');
	}

	public function state()
	{
		return $this->belongsTo(\App\Models\State::class, 'states_id');
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'user_flagging_id');
	}

	public function admin_flags()
	{
		return $this->hasMany(\App\Models\AdminFlag::class, 'flags_id');
	}

	public function assets()
	{
		return $this->belongsToMany(\App\Models\Asset::class, 'flags_assets', 'flags_id', 'assets_id')
					->withTimestamps();
	}

	public function ireports()
	{
		return $this->belongsToMany(\App\Models\Ireport::class, 'flags_ireports', 'flags_id', 'ireports_id')
					->withTimestamps();
	}
}
