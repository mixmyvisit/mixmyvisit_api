<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 11 Oct 2018 19:14:52 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class IreportsAsset
 * 
 * @property int $id
 * @property int $ireports_id
 * @property int $assets_id
 * @property string $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Ireport $ireport
 * @property \App\Models\Asset $asset
 *
 * @package App\Models
 */
class IreportsAsset extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'ireports_id' => 'int',
		'assets_id' => 'int'
	];

	protected $fillable = [
		'ireports_id',
		'assets_id'
	];

	public function ireport()
	{
		return $this->belongsTo(\App\Models\Ireport::class, 'ireports_id');
	}

	public function asset()
	{
		return $this->belongsTo(\App\Models\Asset::class, 'assets_id');
	}
}
