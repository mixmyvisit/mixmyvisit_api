<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 11 Oct 2018 19:14:52 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class MediaAsset
 * 
 * @property int $media_id
 * @property int $assets_id
 * @property string $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Asset $asset
 * @property \App\Models\Medium $medium
 *
 * @package App\Models
 */
class MediaAsset extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	public $incrementing = false;

	protected $casts = [
		'media_id' => 'int',
		'assets_id' => 'int'
	];

	public function asset()
	{
		return $this->belongsTo(\App\Models\Asset::class, 'assets_id');
	}

	public function medium()
	{
		return $this->belongsTo(\App\Models\Medium::class, 'media_id');
	}
}
