<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 11 Oct 2018 19:14:52 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class State
 * 
 * @property int $id
 * @property string $state
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $admin_flags
 * @property \Illuminate\Database\Eloquent\Collection $assets
 * @property \Illuminate\Database\Eloquent\Collection $flags
 * @property \Illuminate\Database\Eloquent\Collection $ireports
 *
 * @package App\Models
 */
class State extends Eloquent
{
	protected $fillable = [
		'state'
	];

	public function admin_flags()
	{
		return $this->hasMany(\App\Models\AdminFlag::class, 'previous_states_id');
	}

	public function assets()
	{
		return $this->hasMany(\App\Models\Asset::class);
	}

	public function flags()
	{
		return $this->hasMany(\App\Models\Flag::class, 'states_id');
	}

	public function ireports()
	{
		return $this->hasMany(\App\Models\Ireport::class, 'states_id');
	}
}
