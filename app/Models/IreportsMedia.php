<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 11 Oct 2018 19:14:52 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class IreportsMedia
 * 
 * @property int $ireports_id
 * @property int $media_id
 * @property string $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Ireport $ireport
 * @property \App\Models\Medium $medium
 *
 * @package App\Models
 */
class IreportsMedia extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	public $incrementing = false;

	protected $casts = [
		'ireports_id' => 'int',
		'media_id' => 'int'
	];

	public function ireport()
	{
		return $this->belongsTo(\App\Models\Ireport::class, 'ireports_id');
	}

	public function medium()
	{
		return $this->belongsTo(\App\Models\Medium::class, 'media_id');
	}
}
