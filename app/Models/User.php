<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 11 Oct 2018 19:14:52 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class User
 *
 * @property int $id
 * @property string $name
 * @property string $firstname
 * @property string $lastname
 * @property string $email
 * @property string $password
 * @property \Carbon\Carbon $birthday
 * @property int $user_types_id
 * @property int $points
 * @property int $occupations_id
 * @property int $profile_picture
 * @property int $user_states_id
 * @property string $verification_token
 * @property \Carbon\Carbon $email_verified_at
 * @property string $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \App\Models\Medium $medium
 * @property \App\Models\Occupation $occupation
 * @property \App\Models\UserState $user_state
 * @property \App\Models\UserType $user_type
 * @property \Illuminate\Database\Eloquent\Collection $admin_blocks_users
 * @property \Illuminate\Database\Eloquent\Collection $admin_flags
 * @property \Illuminate\Database\Eloquent\Collection $flags
 * @property \Illuminate\Database\Eloquent\Collection $ireports
 * @property \Illuminate\Database\Eloquent\Collection $notifications
 * @property \Illuminate\Database\Eloquent\Collection $achievements
 * @property \Illuminate\Database\Eloquent\Collection $assets
 * @property \Illuminate\Database\Eloquent\Collection $challenges
 * @property \Illuminate\Database\Eloquent\Collection $users_media
 *
 * @package App\Models
 */
class User extends Authenticatable
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	use HasApiTokens, Notifiable;

	protected $casts = [
		'user_types_id' => 'int',
		'points' => 'int',
		'occupations_id' => 'int',
		'profile_picture' => 'int',
		'user_states_id' => 'int'
	];

	protected $dates = [
		'birthday',
		'email_verified_at'
	];

	protected $fillable = [
		'name',
		'firstname',
		'lastname',
		'email',
		'password',
		'birthday',
		'user_types_id',
		'points',
		'occupations_id',
		'profile_picture',
		'user_states_id',
		'verification_token',
		'email_verified_at'
	];

	public function medium()
	{
		return $this->belongsTo(\App\Models\Medium::class, 'profile_picture');
	}

	public function occupation()
	{
		return $this->belongsTo(\App\Models\Occupation::class, 'occupations_id');
	}

	public function user_state()
	{
		return $this->belongsTo(\App\Models\UserState::class, 'user_states_id');
	}

	public function user_type()
	{
		return $this->belongsTo(\App\Models\UserType::class, 'user_types_id');
	}

	public function admin_blocks_users()
	{
		return $this->hasMany(\App\Models\AdminBlocksUser::class, 'user');
	}

	public function admin_flags()
	{
		return $this->hasMany(\App\Models\AdminFlag::class, 'users_id');
	}

	public function flags()
	{
		return $this->hasMany(\App\Models\Flag::class, 'user_flagging_id');
	}

	public function ireports()
	{
		return $this->hasMany(\App\Models\Ireport::class, 'users_id');
	}

	public function notifications()
	{
		return $this->belongsToMany(\App\Models\Notification::class, 'notifications_users', 'users_id', 'notifications_id')
					->withPivot('read', 'deleted_at')
					->withTimestamps();
	}

	public function achievements()
	{
		return $this->belongsToMany(\App\Models\Achievement::class, 'users_achievements', 'users_id', 'achievements_id')
					->withPivot('deleted_at')
					->withTimestamps();
	}

	public function assets()
	{
		return $this->belongsToMany(\App\Models\Asset::class, 'users_assets', 'users_id', 'assets_id')
					->withPivot('deleted_at')
					->withTimestamps();
	}

	public function challenges()
	{
		return $this->belongsToMany(\App\Models\Challenge::class, 'users_challenges', 'users_id', 'challenges_id')
					->withPivot('completed', 'ireports_id', 'deleted_at')
					->withTimestamps();
	}

	public function users_media()
	{
		return $this->hasMany(\App\Models\UsersMedia::class, 'users_id');
	}
}
