<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 11 Oct 2018 19:14:52 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Badge
 * 
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int $badge_picture
 * @property string $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Medium $medium
 * @property \Illuminate\Database\Eloquent\Collection $achievements
 * @property \Illuminate\Database\Eloquent\Collection $challenges
 *
 * @package App\Models
 */
class Badge extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'badge_picture' => 'int'
	];

	protected $fillable = [
		'name',
		'description',
		'badge_picture'
	];

	public function medium()
	{
		return $this->belongsTo(\App\Models\Medium::class, 'badge_picture');
	}

	public function achievements()
	{
		return $this->hasMany(\App\Models\Achievement::class, 'achieving_badge');
	}

	public function challenges()
	{
		return $this->hasMany(\App\Models\Challenge::class, 'achieving_badge');
	}
}
