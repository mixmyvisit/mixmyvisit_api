<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 11 Oct 2018 19:14:52 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Platform
 * 
 * @property int $id
 * @property string $name
 * @property string $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $ireports
 *
 * @package App\Models
 */
class Platform extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $fillable = [
		'name'
	];

	public function ireports()
	{
		return $this->belongsToMany(\App\Models\Ireport::class, 'ireports_platforms', 'platforms_id', 'iReports_id')
					->withPivot('description', 'likes', 'comments', 'shares', 'views', 'url', 'deleted_at')
					->withTimestamps();
	}
}
