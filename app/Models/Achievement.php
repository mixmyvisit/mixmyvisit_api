<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 11 Oct 2018 19:14:52 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Achievement
 * 
 * @property int $id
 * @property int $achieving_badge
 * @property string $conditions
 * @property string $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Badge $badge
 * @property \Illuminate\Database\Eloquent\Collection $users
 *
 * @package App\Models
 */
class Achievement extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'achieving_badge' => 'int'
	];

	protected $fillable = [
		'achieving_badge',
		'conditions'
	];

	public function badge()
	{
		return $this->belongsTo(\App\Models\Badge::class, 'achieving_badge');
	}

	public function users()
	{
		return $this->belongsToMany(\App\Models\User::class, 'users_achievements', 'achievements_id', 'users_id')
					->withPivot('deleted_at')
					->withTimestamps();
	}
}
