<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function(Blueprint $table)
		{
			$table->foreign('profile_picture', 'fk_users_media1')->references('id')->on('media')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('occupations_id', 'fk_users_table11')->references('id')->on('occupations')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('user_states_id', 'fk_users_user_states1')->references('id')->on('user_states')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('user_types_id', 'fk_users_user_types1')->references('id')->on('user_types')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users', function(Blueprint $table)
		{
			$table->dropForeign('fk_users_media1');
			$table->dropForeign('fk_users_table11');
			$table->dropForeign('fk_users_user_states1');
			$table->dropForeign('fk_users_user_types1');
		});
	}

}
