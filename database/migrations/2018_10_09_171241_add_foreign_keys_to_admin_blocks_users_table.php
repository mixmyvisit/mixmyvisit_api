<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToAdminBlocksUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('admin_blocks_users', function(Blueprint $table)
		{
			$table->foreign('previous_states_id', 'fk_admin_blocks_users_user_states1')->references('id')->on('user_states')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('admin', 'fk_users_has_users_users3')->references('id')->on('users')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('user', 'fk_users_has_users_users4')->references('id')->on('users')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('admin_blocks_users', function(Blueprint $table)
		{
			$table->dropForeign('fk_admin_blocks_users_user_states1');
			$table->dropForeign('fk_users_has_users_users3');
			$table->dropForeign('fk_users_has_users_users4');
		});
	}

}
