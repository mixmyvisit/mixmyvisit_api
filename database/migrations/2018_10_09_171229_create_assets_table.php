<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAssetsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('assets', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('endpoint', 600);
			$table->integer('state_id')->default(1)->index('fk_videoBites_states1_idx');
			$table->boolean('premium')->default(0)->comment('0 is free
1 is partner
2 is iReporter');
			$table->softDeletes();	
			$table->timestamps();
		});		
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('assets');
	}

}
