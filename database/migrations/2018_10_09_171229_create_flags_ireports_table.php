<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFlagsIreportsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('flags_ireports', function(Blueprint $table)
		{
			$table->integer('flags_id')->index('fk_flags_has_iReports_flags1_idx');
			$table->integer('ireports_id')->index('fk_flags_has_iReports_iReports1_idx');
			$table->primary(['flags_id','ireports_id']);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('flags_ireports');
	}

}
