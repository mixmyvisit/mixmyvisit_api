<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIreportsAssetsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ireports_assets', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('ireports_id')->index('fk_iReports_has_videoBites_iReports_idx');
			$table->integer('assets_id')->index('fk_ireports_assets_assets1_idx');
			$table->softDeletes();	
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ireports_assets');
	}

}
