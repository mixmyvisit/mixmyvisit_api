<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToAdminFlagsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('admin_flags', function(Blueprint $table)
		{
			$table->foreign('previous_states_id', 'fk_admin_flags_states1')->references('id')->on('states')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('flags_id', 'fk_users_flags_flags1')->references('id')->on('flags')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('users_id', 'fk_users_flags_users1')->references('id')->on('users')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('admin_flags', function(Blueprint $table)
		{
			$table->dropForeign('fk_admin_flags_states1');
			$table->dropForeign('fk_users_flags_flags1');
			$table->dropForeign('fk_users_flags_users1');
		});
	}

}
