<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToUsersChallengesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users_challenges', function(Blueprint $table)
		{
			$table->foreign('users_id', 'fk_challenges_has_users_users1')->references('id')->on('users')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('challenges_id', 'fk_challenges_users_challenges1')->references('id')->on('challenges')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('ireports_id', 'fk_users_challenges_ireports1')->references('id')->on('ireports')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users_challenges', function(Blueprint $table)
		{
			$table->dropForeign('fk_challenges_has_users_users1');
			$table->dropForeign('fk_challenges_users_challenges1');
			$table->dropForeign('fk_users_challenges_ireports1');
		});
	}

}
