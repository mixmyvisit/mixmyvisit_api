<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name', 180);
			$table->string('firstname', 180);
			$table->string('lastname', 180);
			$table->string('email', 180)->unique('email_UNIQUE');
			$table->string('password');
			$table->date('birthday');
			$table->integer('user_types_id')->index('fk_users_user_types1_idx');
			$table->integer('points')->default(0);
			$table->integer('occupations_id')->nullable()->index('fk_users_table11_idx');
			$table->integer('profile_picture')->nullable()->index('fk_users_media1_idx');
			$table->integer('user_states_id')->default(1)->index('fk_users_user_states1_idx');
			$table->string('verification_token')->nullable();
			$table->dateTime('email_verified_at')->nullable();
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
