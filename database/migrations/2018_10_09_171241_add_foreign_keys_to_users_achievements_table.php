<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToUsersAchievementsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users_achievements', function(Blueprint $table)
		{
			$table->foreign('achievements_id', 'fk_users_has_achievements_achievements1')->references('id')->on('achievements')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('users_id', 'fk_users_has_achievements_users1')->references('id')->on('users')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users_achievements', function(Blueprint $table)
		{
			$table->dropForeign('fk_users_has_achievements_achievements1');
			$table->dropForeign('fk_users_has_achievements_users1');
		});
	}

}
