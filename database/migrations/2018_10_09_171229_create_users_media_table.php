<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersMediaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users_media', function(Blueprint $table)
		{
			$table->integer('users_id')->index('fk_users_has_media_users1_idx');
			$table->integer('media_id')->index('fk_users_has_media_media1_idx');
			$table->primary(['users_id','media_id']);
			$table->softDeletes();	
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users_media');
	}

}
