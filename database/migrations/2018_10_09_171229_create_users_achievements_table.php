<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersAchievementsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users_achievements', function(Blueprint $table)
		{
			$table->integer('users_id')->index('fk_users_has_achievements_users1_idx');
			$table->integer('achievements_id')->index('fk_users_has_achievements_achievements1_idx');
			$table->primary(['users_id','achievements_id']);
			$table->softDeletes();	
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users_achievements');
	}

}
