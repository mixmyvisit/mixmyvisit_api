<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToIreportsVideosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('ireports_videos', function(Blueprint $table)
		{
			$table->foreign('ireports_id', 'fk_ireports_has_media_ireports2')->references('id')->on('ireports')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('media_id', 'fk_ireports_has_media_media2')->references('id')->on('media')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('ireports_videos', function(Blueprint $table)
		{
			$table->dropForeign('fk_ireports_has_media_ireports2');
			$table->dropForeign('fk_ireports_has_media_media2');
		});
	}

}
