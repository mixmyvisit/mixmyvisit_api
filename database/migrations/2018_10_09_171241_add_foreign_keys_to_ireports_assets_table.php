<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToIreportsAssetsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('ireports_assets', function(Blueprint $table)
		{
			$table->foreign('ireports_id', 'fk_iReports_assets_iReports')->references('id')->on('ireports')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('assets_id', 'fk_ireports_assets_assets1')->references('id')->on('assets')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('ireports_assets', function(Blueprint $table)
		{
			$table->dropForeign('fk_iReports_assets_iReports');
			$table->dropForeign('fk_ireports_assets_assets1');
		});
	}

}
