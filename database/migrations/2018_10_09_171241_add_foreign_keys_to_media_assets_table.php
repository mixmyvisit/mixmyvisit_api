<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMediaAssetsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('media_assets', function(Blueprint $table)
		{
			$table->foreign('assets_id', 'fk_media_users_assets1')->references('id')->on('assets')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('media_id', 'fk_media_users_media1')->references('id')->on('media')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('media_assets', function(Blueprint $table)
		{
			$table->dropForeign('fk_media_users_assets1');
			$table->dropForeign('fk_media_users_media1');
		});
	}

}
