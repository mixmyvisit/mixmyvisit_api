<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersAssetsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users_assets', function(Blueprint $table)
		{
			$table->integer('users_id')->index('fk_users_has_videoBites_users1_idx');
			$table->integer('assets_id')->index('fk_users_assets_assets1_idx');
			$table->primary(['users_id','assets_id']);
			$table->softDeletes();	
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users_assets');
	}

}
