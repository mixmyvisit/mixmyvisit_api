<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToChallengesMediaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('challenges_media', function(Blueprint $table)
		{
			$table->foreign('media_id', 'fk_challenges_has_media_media1')->references('id')->on('media')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('challenges_id', 'fk_challenges_media_challenges2')->references('id')->on('challenges')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('challenges_media', function(Blueprint $table)
		{
			$table->dropForeign('fk_challenges_has_media_media1');
			$table->dropForeign('fk_challenges_media_challenges2');
		});
	}

}
