<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersChallengesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users_challenges', function(Blueprint $table)
		{
			$table->integer('challenges_id')->index('fk_challenges_has_users_challenges1_idx');
			$table->integer('users_id')->index('fk_challenges_has_users_users1_idx');
			$table->boolean('completed')->default(0);
			$table->integer('ireports_id')->nullable()->index('fk_users_challenges_ireports1_idx');
			$table->primary(['challenges_id','users_id']);
			$table->softDeletes();	
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users_challenges');
	}

}
