<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToChallengesAssetsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('challenges_assets', function(Blueprint $table)
		{
			$table->foreign('assets_id', 'fk_challenges_assets_assets1')->references('id')->on('assets')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('challenges_id', 'fk_challenges_has_media_challenges1')->references('id')->on('challenges')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('challenges_assets', function(Blueprint $table)
		{
			$table->dropForeign('fk_challenges_assets_assets1');
			$table->dropForeign('fk_challenges_has_media_challenges1');
		});
	}

}
