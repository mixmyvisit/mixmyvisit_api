<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToFlagsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('flags', function(Blueprint $table)
		{
			$table->foreign('flags_types_id', 'fk_flags_flags_types1')->references('id')->on('flags_types')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('states_id', 'fk_flags_states1')->references('id')->on('states')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('user_flagging_id', 'fk_flags_users2')->references('id')->on('users')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('flags', function(Blueprint $table)
		{
			$table->dropForeign('fk_flags_flags_types1');
			$table->dropForeign('fk_flags_states1');
			$table->dropForeign('fk_flags_users2');
		});
	}

}
