<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateChallengesMediaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('challenges_media', function(Blueprint $table)
		{
			$table->integer('challenges_id')->index('fk_challenges_has_media_challenges2_idx');
			$table->integer('media_id')->index('fk_challenges_has_media_media1_idx');
			$table->primary(['challenges_id','media_id']);
			$table->softDeletes();	
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('challenges_media');
	}

}
