<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToNotificationsUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('notifications_users', function(Blueprint $table)
		{
			$table->foreign('notifications_id', 'fk_notifications_has_users_notifications1')->references('id')->on('notifications')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('users_id', 'fk_notifications_has_users_users1')->references('id')->on('users')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('notifications_users', function(Blueprint $table)
		{
			$table->dropForeign('fk_notifications_has_users_notifications1');
			$table->dropForeign('fk_notifications_has_users_users1');
		});
	}

}
