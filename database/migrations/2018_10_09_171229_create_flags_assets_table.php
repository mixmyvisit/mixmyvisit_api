<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFlagsAssetsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('flags_assets', function(Blueprint $table)
		{
			$table->integer('flags_id')->index('fk_flags_has_assets_flags1_idx');
			$table->integer('assets_id')->index('fk_flags_has_assets_assets1_idx');
			$table->primary(['flags_id','assets_id']);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('flags_assets');
	}

}
