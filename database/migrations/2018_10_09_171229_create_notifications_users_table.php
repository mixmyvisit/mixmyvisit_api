<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNotificationsUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('notifications_users', function(Blueprint $table)
		{
			$table->integer('notifications_id')->index('fk_notifications_has_users_notifications1_idx');
			$table->integer('users_id')->index('fk_notifications_has_users_users1_idx');
			$table->boolean('read')->default(0);
			$table->primary(['notifications_id','users_id']);
			$table->softDeletes();	
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('notifications_users');
	}

}
