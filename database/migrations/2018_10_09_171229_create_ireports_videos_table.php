<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIreportsVideosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ireports_videos', function(Blueprint $table)
		{
			$table->integer('ireports_id')->index('fk_ireports_has_media_ireports2_idx');
			$table->integer('media_id')->index('fk_ireports_has_media_media2_idx');
			$table->primary(['ireports_id','media_id']);
			$table->softDeletes();	
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ireports_videos');
	}

}
