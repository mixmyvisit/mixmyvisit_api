<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToIreportsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('ireports', function(Blueprint $table)
		{
			$table->foreign('states_id', 'fk_iReports_states1')->references('id')->on('states')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('users_id', 'fk_iReports_users1')->references('id')->on('users')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('ireports', function(Blueprint $table)
		{
			$table->dropForeign('fk_iReports_states1');
			$table->dropForeign('fk_iReports_users1');
		});
	}

}
