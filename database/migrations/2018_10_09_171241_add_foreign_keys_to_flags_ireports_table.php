<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToFlagsIreportsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('flags_ireports', function(Blueprint $table)
		{
			$table->foreign('flags_id', 'fk_flags_iReports_flags1')->references('id')->on('flags')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('ireports_id', 'fk_flags_iReports_iReports1')->references('id')->on('ireports')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('flags_ireports', function(Blueprint $table)
		{
			$table->dropForeign('fk_flags_iReports_flags1');
			$table->dropForeign('fk_flags_iReports_iReports1');
		});
	}

}
