<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdminBlocksUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('admin_blocks_users', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('admin')->index('fk_users_has_users_users3_idx');
			$table->integer('user')->index('fk_users_has_users_users4_idx');
			$table->integer('previous_states_id')->index('fk_admin_blocks_users_user_states1_idx');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('admin_blocks_users');
	}

}
