<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToFlagsAssetsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('flags_assets', function(Blueprint $table)
		{
			$table->foreign('assets_id', 'fk_flags_assets_assets1')->references('id')->on('assets')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('flags_id', 'fk_flags_assets_flags1')->references('id')->on('flags')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('flags_assets', function(Blueprint $table)
		{
			$table->dropForeign('fk_flags_assets_assets1');
			$table->dropForeign('fk_flags_assets_flags1');
		});
	}

}
