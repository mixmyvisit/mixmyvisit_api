<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateChallengesAssetsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('challenges_assets', function(Blueprint $table)
		{
			$table->integer('challenges_id')->index('fk_challenges_has_media_challenges1_idx');
			$table->integer('assets_id')->index('fk_challenges_videobites_assets1_idx');
			$table->primary(['challenges_id','assets_id']);
			$table->softDeletes();	
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('challenges_assets');
	}

}
