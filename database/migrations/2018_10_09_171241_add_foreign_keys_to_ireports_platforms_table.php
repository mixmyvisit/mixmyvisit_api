<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToIreportsPlatformsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('ireports_platforms', function(Blueprint $table)
		{
			$table->foreign('iReports_id', 'fk_iReports_has_platforms_iReports1')->references('id')->on('ireports')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('platforms_id', 'fk_iReports_has_platforms_platforms1')->references('id')->on('platforms')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('ireports_platforms', function(Blueprint $table)
		{
			$table->dropForeign('fk_iReports_has_platforms_iReports1');
			$table->dropForeign('fk_iReports_has_platforms_platforms1');
		});
	}

}
