<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserStatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_states', function(Blueprint $table)
		{
			$table->integer('id', true)->comment('0 BLOCKED Nao pode produzir
1 ACTIVE OK
2 BANNED Bloqueio geral');
			$table->string('state', 45);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_states');
	}

}
