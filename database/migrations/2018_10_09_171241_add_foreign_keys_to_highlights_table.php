<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToHighlightsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('highlights', function(Blueprint $table)
		{
			$table->foreign('ireports_id', 'fk_highlights_ireports1')->references('id')->on('ireports')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('highlights', function(Blueprint $table)
		{
			$table->dropForeign('fk_highlights_ireports1');
		});
	}

}
