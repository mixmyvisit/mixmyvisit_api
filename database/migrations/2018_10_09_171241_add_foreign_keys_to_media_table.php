<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMediaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('media', function(Blueprint $table)
		{
			$table->foreign('assets_id', 'fk_media_assets1')->references('id')->on('assets')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('media_types_id', 'fk_media_media_types1')->references('id')->on('media_types')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('media', function(Blueprint $table)
		{
			$table->dropForeign('fk_media_assets1');
			$table->dropForeign('fk_media_media_types1');
		});
	}

}
