<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdminFlagsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('admin_flags', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('users_id')->index('fk_users_has_flags_users1_idx');
			$table->integer('flags_id')->index('fk_users_has_flags_flags1_idx');
			$table->integer('previous_states_id')->index('fk_admin_flags_states1_idx');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('admin_flags');
	}

}
