<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMediaAssetsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('media_assets', function(Blueprint $table)
		{
			$table->integer('media_id');
			$table->integer('assets_id')->index('fk_media_users_assets1_idx');
			$table->primary(['media_id','assets_id']);
			$table->softDeletes();	
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('media_assets');
	}

}
