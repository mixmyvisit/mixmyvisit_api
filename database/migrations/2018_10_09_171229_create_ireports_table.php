<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIreportsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ireports', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name', 45);
			$table->text('video_details');
			$table->integer('users_id')->index('fk_iReports_users1_idx');
			$table->integer('states_id')->index('fk_iReports_states1_idx');
			$table->softDeletes();	
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ireports');
	}

}
