<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFlagsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('flags', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('flags_types_id')->index('fk_flags_flags_types1_idx');
			$table->integer('user_flagging_id')->index('fk_flags_users2_idx');
			$table->string('description', 300)->nullable();
			$table->integer('states_id')->index('fk_flags_states1_idx');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('flags');
	}

}
