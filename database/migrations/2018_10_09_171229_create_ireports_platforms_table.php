<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIreportsPlatformsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ireports_platforms', function(Blueprint $table)
		{
			$table->integer('iReports_id')->index('fk_iReports_has_platforms_iReports1_idx');
			$table->integer('platforms_id')->index('fk_iReports_has_platforms_platforms1_idx');
			$table->string('description', 300)->nullable();
			$table->integer('likes');
			$table->integer('comments');
			$table->integer('shares');
			$table->bigInteger('views');
			$table->string('url', 1000)->nullable();
			$table->primary(['iReports_id','platforms_id']);
			$table->softDeletes();	
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ireports_platforms');
	}

}
